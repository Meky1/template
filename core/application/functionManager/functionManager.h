/*****************************************************************************
 * @file functionManager.h
 *
 * @brief Header file containing the setup starting function and the loop function
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/** @brief 	Initialize system function
 */
void FunctionManagementSetup(void);

/** @brief 	System loop function
 */
void FunctionManagementLoop(void);
