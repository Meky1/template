/*****************************************************************************
 * @file functionManager.c
 *
 * @brief Source file containing the setup starting function and the loop function
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/gpioManagement/gpioManagement.h"
#include "main.h"

#include "application/functionManager/functionManager.h"
#include "driver/flashDriver/flashDriver.h"
#include "middleware/dataConfig/dataConfig.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/


void FunctionManagementSetup(void)
{
    dataConfigListInit();
    // dataConfigClearAllConfig();
    for (int i = 0; i < 1000; i++) {
        LedToggleRedLed();
        HAL_Delay(200);
    }
    // JumpToBootloader();
}

void FunctionManagementLoop(void)
{
}
