/*****************************************************************************
 * @file dataConfig.c
 *
 * @brief Source file of the data config
 *
 * @author Marcin Czarnik
 * @date 28.07.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/gpioManagement/gpioManagement.h"
#include "main.h"
#include <stdlib.h>

#include "driver/flashDriver/flashDriver.h"
#include "middleware/dataConfig/dataConfig.h"

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

typedef struct dataObject {
    uint32_t address;
    uint32_t value;
    listOfConfigOptions_t option;
    struct dataObject *next;
} dataObject_t;

static dataObject_t *head;

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/** @brief 	Find the object using option name
 *  @return The object found
 */
static dataObject_t *FindConfigByName(listOfConfigOptions_t option);

/** @brief 	Reload the flash list in order to edit data by deleting the whole page and flashing the memory from the list data
 *  @return True if everything ok; otherwise false
 */
static bool ReloadFlashList(void);

/** @brief 	Program the object into the flash
 *  @return True if everything ok; otherwise false
 */
static bool ProgramObject(dataObject_t *object);

/** @brief 	Download the data from the flash and create a list
 */
static void FillUpWithFlashData(void);

/** @brief 	Read data from a single configuration
 */
static void ReadNewConfig(listOfConfigOptions_t option, uint32_t address);

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

bool dataConfigListInit(void)
{
    head = (dataObject_t *)malloc(sizeof(dataObject_t));
    head = NULL;

    FillUpWithFlashData();
    if (!ReloadFlashList()) {
        return false;
    }

    return true;
}

bool dataConfigSetExistingConfig(uint32_t value, listOfConfigOptions_t option)
{
    dataObject_t *object = FindConfigByName(option);
    object->value = value;

    if (!ReloadFlashList()) {
        return false;
    }

    return true;
}

uint32_t dataConfigGetConfig(listOfConfigOptions_t option)
{
    dataObject_t *object = FindConfigByName(option);

    return object->value;
}

bool dataConfigClearAllConfig(void)
{
    dataObject_t *current = head;
    while (current != NULL) {
        current->value = 0x00;
        current = current->next;
    }

    if (!ReloadFlashList()) {
        return false;
    }
    return true;
}

/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/

static dataObject_t *FindConfigByName(listOfConfigOptions_t option)
{
    if (head == NULL) {
        return NULL;
    }

    dataObject_t *current = head;

    while (current != NULL) {
        if (current->option == option) {
            return current;
        }
        current = current->next;
    }
    return NULL;
}

static bool ReloadFlashList(void)
{
    if (flashErasePage() != FLASH_OK) {
        return false;
    }

    dataObject_t *current = head;

    while (current != NULL) {
        if (!ProgramObject(current)) {
            return false;
        }
        current = current->next;
    }
    return true;
}

static bool ProgramObject(dataObject_t *object)
{
    if (flashWrite(object->address, (uint32_t *)&object->address, 1) != FLASH_OK) {
        return false;
    }
    if (flashWrite(object->address + 0x4, (uint32_t *)&object->value, 1) != FLASH_OK) {
        return false;
    }

    return true;
}

static void FillUpWithFlashData(void)
{
    uint32_t address = FLASH_CONFIG_DATA_START_ADDRESS;

    for (int i = 0; i < NUMBER_OF_CONFIGS; i++) {
        ReadNewConfig(i, address);
        address = address + 0x8;
    }
}

static void ReadNewConfig(listOfConfigOptions_t option, uint32_t address)
{
    uint32_t valueAddress = address + 0x4;

    if (head == NULL) {

        head = (dataObject_t *)malloc(sizeof(dataObject_t));
        head->value = (*(volatile uint32_t *)valueAddress);
        head->option = option;
        head->address = address;
        head->next = NULL;
    }
    else {
        dataObject_t *current = head;

        while (current->next != NULL) {
            current = current->next;
        }

        current->next = (dataObject_t *)malloc(sizeof(dataObject_t));
        current = current->next;

        current->value = (*(volatile uint32_t *)valueAddress);
        current->option = option;
        current->address = address;
        current->next = NULL;
    }
}
