/*****************************************************************************
 * @file dataConfig.h
 *
 * @brief Header file of the data config
 *
 * @author Marcin Czarnik
 * @date 28.07.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

typedef enum {
    CONFIG1 = 0,
    CONFIG2,
    CONFIG3,
    CONFIG4,
    CONFIG5,
    CONFIG6,
    CONFIG7,
    CONFIG8,
    CONFIG9,
    CONFIG10,
    CONFIG11,
    CONFIG12,
    CONFIG13,
    CONFIG14,
    CONFIG15,
    CONFIG16,
    CONFIG17,
    CONFIG18,
    CONFIG19,
    CONFIG20,
    CONFIG21,
    CONFIG22,
    NUMBER_OF_CONFIGS


} listOfConfigOptions_t;

/** @brief 	Initialize list function
 *  @return True if everything ok; otherwise false
 */
bool dataConfigListInit(void);

/** @brief 	Set a certain configuration in the flash memory by using the list interface
 *  @return True if everything ok; otherwise false
 */
bool dataConfigSetExistingConfig(uint32_t value, listOfConfigOptions_t option);

/** @brief 	Get a certain configuration in the flash memory by using the list interface
 *  @return The value
 */
uint32_t dataConfigGetConfig(listOfConfigOptions_t option);

/** @brief  Clear all values in the list and the flash
 *  @return True if everything ok; otherwise false
 */
bool dataConfigClearAllConfig(void);
