/*****************************************************************************
 * @file gpioManagement.c
 *
 * @brief Source file of the gpio peripherals
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/gpioManagement/gpioManagement.h"
#include "main.h"

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
 *****************************************************************************/

void LedTurnOnGreenLed(void)
{
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_SET);
}

void LedTurnOffGreenLed(void)
{
    HAL_GPIO_WritePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin, GPIO_PIN_RESET);
}

void LedToggleGreenLed(void)
{
    HAL_GPIO_TogglePin(LED_GREEN_GPIO_Port, LED_GREEN_Pin);
}

void LedTurnOnRedLed(void)
{
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_SET);
}

void LedTurnOffRedLed(void)
{
    HAL_GPIO_WritePin(LED_RED_GPIO_Port, LED_RED_Pin, GPIO_PIN_RESET);
}

void LedToggleRedLed(void)
{
    HAL_GPIO_TogglePin(LED_RED_GPIO_Port, LED_RED_Pin);
}


bool IsUnavailabilityFlagHigh(void)
{
    if (HAL_GPIO_ReadPin(UNAVAILABLE_TIME_GPIO_Port, UNAVAILABLE_TIME_Pin) == GPIO_PIN_SET) {
        return true;
    }
    return false;
}
