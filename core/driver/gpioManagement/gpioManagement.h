/*****************************************************************************
 * @file gpioManagement.h
 *
 * @brief Header file of the gpio peripherals
 *
 * @author Marcin Czarnik
 * @date 05.03.2020
 * @version v1.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdbool.h>
#include <stdio.h>

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/** @brief 	Turn on the green LED
 */
void LedTurnOnGreenLed(void);

/** @brief 	Turn off the green LED
 */
void LedTurnOffGreenLed(void);

/** @brief Toggle the green LED
 */
void LedToggleGreenLed(void);

/** @brief 	Turn on the red LED
 */
void LedTurnOnRedLed(void);

/** @brief 	Turn off the red LED
 */
void LedTurnOffRedLed(void);

/** @brief Toggle the red LED
 */
void LedToggleRedLed(void);

/** @brief 	Check if the unavailability flag is high or low
 */
bool IsUnavailabilityFlagHigh(void);
