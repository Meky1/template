/*****************************************************************************
 * @file flashDriver.h
 *
 * @brief Header file for the flash driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#pragma once

#include <stdint.h>

/*****************************************************************************
                       PUBLIC DEFINES / MACROS / ENUMS
*****************************************************************************/

/* Start and end addresses of the user application. */
#define FLASH_CONFIG_DATA_START_ADDRESS ((uint32_t)0x08010000)
#define FLASH_CONFIG_DATA_END_ADDRESS ((uint32_t)0x08018000 - 1U)
#define FLASH_CONFIG_DATA_SIZE (FLASH_CONFIG_DATA_START_ADDRESS - FLASH_CONFIG_DATA_END_ADDRESS)

/* Status report for the functions. */
typedef enum {
    FLASH_OK = 0x00u,          /**< The action was successful. */
    FLASH_SIZE_ERROR = 0x01u,  /**< The binary is too big. */
    FLASH_WRITE_ERROR = 0x02u, /**< Writing failed. */
    FLASH_READBACK_ERROR = 0x04u, /**< Writing was successful, but the content of the memory is wrong. */
    FLASH_ERROR = 0xFFu           /**< Generic error. */
} flashStatus_t;

/*****************************************************************************
                         PUBLIC INTERFACE DECLARATION
 *****************************************************************************/

/**
 * @brief   This function erases the page nr 11.
 * @return  status: Report about the success of the erasing.
 */
flashStatus_t flashErasePage(void);

/**
 * @brief   This function flashes the memory.
 * @param   address: First address to be written to.
 * @param   *data:   Array of the data that we want to write.
 * @param   *length: Size of the array.
 * @return  status: Report about the success of the writing.
 */
flashStatus_t flashWrite(uint32_t address, uint32_t *data, uint32_t length);


void JumpToBootloader(void) __attribute__((optimize("-O0")));
;
