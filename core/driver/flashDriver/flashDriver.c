/*****************************************************************************
 * @file flashDriver.c
 *
 * @brief Source file for the flash driver
 *
 * @author Marcin Czarnik
 * @date 29.05.2020
 * @version v2.0
 *
 * @copyright 2019 Marcin Czarnik - all rights reserved.
 ****************************************************************************/

#include "driver/flashDriver/flashDriver.h"
#include "main.h"

/*****************************************************************************
                          PRIVATE DEFINES / MACROS
*****************************************************************************/

/*****************************************************************************
                     PRIVATE STRUCTS / ENUMS / VARIABLES
*****************************************************************************/

/*****************************************************************************
                         PRIVATE FUNCTION DECLARATION
 *****************************************************************************/

/*****************************************************************************
                           INTERFACE IMPLEMENTATION
*****************************************************************************/

flashStatus_t flashErasePage(void)
{
    flashStatus_t status = FLASH_ERROR;
    uint32_t UserStartSector;
    uint32_t NumberOfSectors;
    uint32_t SectorError;
    FLASH_EraseInitTypeDef pEraseInit;

    /* Unlock the Flash to enable the flash control register access *************/
    HAL_FLASH_Unlock();

    /* Get the sector where start the user flash area */
    UserStartSector = FLASH_SECTOR_11;
    NumberOfSectors = 1;

    pEraseInit.TypeErase = FLASH_TYPEERASE_SECTORS;
    pEraseInit.Sector = UserStartSector;
    pEraseInit.NbSectors = NumberOfSectors;
    pEraseInit.VoltageRange = VOLTAGE_RANGE_3;

    if (HAL_FLASHEx_Erase(&pEraseInit, &SectorError) == HAL_OK) {
        /* Error occurred while page erase */
        status = FLASH_OK;
        FLASH->CR;
    }

    HAL_FLASH_Lock();

    return status;
}

flashStatus_t flashWrite(uint32_t address, uint32_t *data, uint32_t length)
{
    flashStatus_t status = FLASH_OK;

    HAL_FLASH_Unlock();

    /* Loop through the array. */
    for (uint32_t i = 0; (i < length) && (FLASH_OK == status); i++) {
        /* If we reached the end of the memory, then report an error and don't do anything else.*/
        if (FLASH_CONFIG_DATA_END_ADDRESS <= address) {
            status |= FLASH_SIZE_ERROR;
        }
        else {
            /* The actual flashing. If there is an error, then report it. */
            if (HAL_OK != HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, address, data[i])) {
                status |= FLASH_WRITE_ERROR;
            }
            /* Read back the content of the memory. If it is wrong, then report an error. */
            if (((data[i])) != (*(volatile uint32_t *)address)) {
                status |= FLASH_READBACK_ERROR;
            }

            /* Shift the address by a word. */
            address += 4u;
        }
    }

    CLEAR_BIT(FLASH->CR, (FLASH_CR_PG));
    HAL_FLASH_Lock();

    return status;
}

void JumpToBootloader(void)
{
    void (*SysMemBootJump)(void);

    /* Set the address of the entry point to bootloader */
    volatile uint32_t BootAddr = 0x08000000;

    /* Disable all interrupts */
    __disable_irq();

    /* Disable Systick timer */
    SysTick->CTRL = 0;

    /* Set the clock to the default state */
    HAL_RCC_DeInit();

    /* Clear Interrupt Enable Register & Interrupt Pending Register */
    for (uint8_t i = 0; i < 8; i++) {
        NVIC->ICER[i] = 0xFFFFFFFF;
        NVIC->ICPR[i] = 0xFFFFFFFF;
    }

    /* Re-enable all interrupts */
    __enable_irq();

    /* Set up the jump to booloader address + 4 */
    SysMemBootJump = (void (*)(void))(*((uint32_t *)((BootAddr + 4))));

    /* Set the main stack pointer to the bootloader stack */
    __set_MSP(*(uint32_t *)BootAddr);

    /* Call the function to jump to bootloader location */
    SysMemBootJump();

    /* Jump is done successfully */
    while (1) {
        /* Code should never reach this loop */
    }
}


/******************************************************************************
                        PRIVATE FUNCTION IMPLEMENTATION
 ******************************************************************************/
